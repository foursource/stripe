package stripe

import (
	stripe "bitbucket.org/foursource/stripe/lib"
	"bitbucket.org/foursource/stripe/lib/webhook"
)

func ConstructEvent(payload []byte, header string, secret string) (*stripe.Event, error) {
	event, err := webhook.ConstructEventIgnoringTolerance(payload, header, secret)
	return &event, err
}
