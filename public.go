package stripe

import (
	stripe "bitbucket.org/foursource/stripe/lib"
)

func FindCustomer(email string) (*stripe.Customer, error) {
	return defaultClient.FindCustomer(email)
}

func CreateCustomer(params *stripe.CustomerParams) (*stripe.Customer, error) {
	return defaultClient.CreateCustomer(params)
}

func UpdateCustomer(id string, params *stripe.CustomerParams) (*stripe.Customer, error) {
	return defaultClient.UpdateCustomer(id, params)
}

func CreatePayment(cus *stripe.Customer, amount float64) (*stripe.PaymentIntent, error) {
	return defaultClient.CreatePayment(cus, amount)
}

func ConfirmPayment(pi *stripe.PaymentIntent) (*stripe.PaymentIntent, string, error) {
	return defaultClient.ConfirmPayment(pi)
}

func CancelPayment(id string, cancelReason stripe.PaymentIntentCancellationReason) (*stripe.PaymentIntent, error) {
	return defaultClient.CancelPayment(id, cancelReason)
}
