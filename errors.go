package stripe

import "fmt"

var (
	ErrorNoPaymentLinkAvailable = fmt.Errorf("no payment link available")
)
