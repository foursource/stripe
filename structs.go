package stripe

import (
	"bitbucket.org/foursource/stripe/lib/customer"
	"bitbucket.org/foursource/stripe/lib/paymentintent"
)

type (
	Client struct {
		customer             customer.Client
		paymentIntentPublic  paymentintent.Client
		paymentIntentPrivate paymentintent.Client
		logFunction          logFunction
		config               *Config
	}

	Config struct {
		ApiPublicKey string
		ApiSecretKey string
		ReturnUrl    string
		Enabled      bool
	}

	logFunction func(err error) error
)
