package stripe

import (
	"fmt"
	stripe "bitbucket.org/foursource/stripe/lib"
	"bitbucket.org/foursource/stripe/lib/customer"
	"bitbucket.org/foursource/stripe/lib/paymentintent"
)

func Init(config *Config, options ...Option) {
	backend := stripe.GetBackend(stripe.APIBackend)

	defaultClient = &Client{
		customer: customer.Client{
			B:   backend,
			Key: config.ApiSecretKey,
		},
		paymentIntentPrivate: paymentintent.Client{
			B:   backend,
			Key: config.ApiSecretKey,
		},
		paymentIntentPublic: paymentintent.Client{
			B:   backend,
			Key: config.ApiPublicKey,
		},
		logFunction:     defaultLogFunction(),
		config:          config,
	}

	defaultClient.Reconfigure(options...)

	if defaultClient.IsEnabled() {
		printInfo("Enabled")
	} else {
		printError(fmt.Errorf("Disabled"))
	}
}

func (c *Client) IsEnabled() bool {
	return c.config.Enabled
}
