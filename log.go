package stripe

import "fmt"

func defaultLogFunction() logFunction {
	return func(err error) error {
		printError(err)

		return err
	}
}

func printInfo(message string, argument ...interface{}) {
	fmt.Println(fmt.Sprintf(":: %sStripe%s :: %s", colorGreen, colorReset, fmt.Sprintf(message, argument...)))
}

func printError(err error) {
	fmt.Println(fmt.Sprintf(":: %sStripe%s :: %s", colorRed, colorReset, err))
}
