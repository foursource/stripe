package stripe

// Option ...
type Option func(client *Client)

// Reconfigure ...
func (c *Client) Reconfigure(options ...Option) {
	for _, option := range options {
		option(c)
	}
}

// WithLogFunction ...
func WithLogFunction(f logFunction) Option {
	return func(c *Client) {
		c.logFunction = f
	}
}
