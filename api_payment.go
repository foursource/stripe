package stripe

import (
	"net/url"
	stripe "bitbucket.org/foursource/stripe/lib"
)

func (c *Client) CreatePayment(cus *stripe.Customer, amount float64) (*stripe.PaymentIntent, error) {
	if !c.IsEnabled() {
		return &stripe.PaymentIntent{}, nil
	}

	params := &stripe.PaymentIntentParams{
		Amount:   stripe.Int64(int64(amount * 100)),
		Currency: stripe.String(string(stripe.CurrencyEUR)),
		PaymentMethodTypes: []*string{
			stripe.String(aliPayType),
		},
	}

	if cus != nil {
		params.Customer = stripe.String(cus.ID)
	}

	newPaymentIntent, err := c.paymentIntentPrivate.New(params)
	if err != nil {
		return nil, c.logFunction(err)
	}

	return newPaymentIntent, nil
}

func (c *Client) ConfirmPayment(pi *stripe.PaymentIntent) (*stripe.PaymentIntent, string, error) {
	if !c.IsEnabled() {
		return &stripe.PaymentIntent{}, "", nil
	}

	params := &stripe.PaymentIntentConfirmParams{
		ReturnURL: stripe.String(c.config.ReturnUrl),
		Params: stripe.Params{
			Extra: &stripe.ExtraValues{
				Values: url.Values{
					"expected_payment_method_type": []string{
						aliPayType,
					},
					"client_secret": []string{pi.ClientSecret},
				},
			},
		},
	}

	updPaymentIntent, err := c.paymentIntentPublic.Confirm(pi.ID, params)
	if err != nil {
		return nil, "", c.logFunction(err)
	}

	if updPaymentIntent.NextAction != nil &&
		updPaymentIntent.NextAction.AlipayHandleRedirect != nil &&
		updPaymentIntent.NextAction.AlipayHandleRedirect.URL != "" {
		return updPaymentIntent, updPaymentIntent.NextAction.AlipayHandleRedirect.URL, nil
	}

	return nil, "", ErrorNoPaymentLinkAvailable
}

func (c *Client) CancelPayment(id string, cancelReason stripe.PaymentIntentCancellationReason) (*stripe.PaymentIntent, error) {
	if !c.IsEnabled() {
		return &stripe.PaymentIntent{}, nil
	}

	params := &stripe.PaymentIntentCancelParams{
		CancellationReason: stripe.String(string(cancelReason)),
	}

	pi, err := c.paymentIntentPrivate.Cancel(id, params)
	if err != nil {
		return nil, c.logFunction(err)
	}

	return pi, nil
}
