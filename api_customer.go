package stripe

import (
	stripe "bitbucket.org/foursource/stripe/lib"
)

func (c *Client) FindCustomer(email string) (*stripe.Customer, error) {
	if !c.IsEnabled() {
		return &stripe.Customer{}, nil
	}

	params := &stripe.CustomerListParams{
		Email: stripe.String(email),
	}

	iter := c.customer.List(params)
	if iter.Next() {
		if err := iter.Err(); err != nil {
			return nil, err
		}
		return iter.Customer(), nil
	}

	return nil, nil
}

func (c *Client) CreateCustomer(params *stripe.CustomerParams) (*stripe.Customer, error) {
	if !c.IsEnabled() {
		return &stripe.Customer{}, nil
	}

	newCustomer, err := c.customer.New(params)
	if err != nil {
		return nil, c.logFunction(err)
	}

	return newCustomer, nil
}

func (c *Client) UpdateCustomer(id string, params *stripe.CustomerParams) (*stripe.Customer, error) {
	if !c.IsEnabled() {
		return &stripe.Customer{}, nil
	}

	updCustomer, err := c.customer.Update(id, params)
	if err != nil {
		return nil, c.logFunction(err)
	}

	return updCustomer, nil
}
