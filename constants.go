package stripe

const (
	colorGreen = "\033[32m"
	colorRed   = "\033[31m"
	colorReset = "\033[0m"

	aliPayType   = "alipay"
)
