# README

The files at "/lib" folder are a fork from github project [stripe-go](https://github.com/stripe/stripe-go).

Replaced imports from "github.com/stripe/stripe-go" to "bitbucket.org/foursource/stripe/lib"

Added 'AlipayHandleRedirect' at 'PaymentIntentNextAction'
```
// PaymentIntentNextAction represents the type of action to take on a payment intent.
type PaymentIntentNextAction struct {
	RedirectToURL        *PaymentIntentNextActionRedirectToURL `json:"redirect_to_url"`
	AlipayHandleRedirect *PaymentIntentNextActionRedirectToURL `json:"alipay_handle_redirect"`
	Type                 PaymentIntentNextActionType           `json:"type"`
}
```

### Repository
This is a repo to use as submodule. Please read: [here](https://git-scm.com/book/en/v2/Git-Tools-Submodules)